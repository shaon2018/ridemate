package com.lastblade.ridemate.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.AnimationUtils
import com.lastblade.ridemate.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /*Full screen splash screen*/
        this.window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_splash)

        logoBounceEffectAnim()
        rightBottomLogoAnim()
        /* Waiting for splash screen*/
        Handler().postDelayed({ startActivity() }, 1200)
    }

    /** Implemented bounce effect for center logo*/
    private fun logoBounceEffectAnim() {
        val anim = AnimationUtils.loadAnimation(this, R.anim.bounce_effect)
        ivSplashRideMateLogo.startAnimation(anim)
    }

    /** Implemented slide from bottom animation for right bottom logo*/
    private fun rightBottomLogoAnim() {
        val anim = AnimationUtils.loadAnimation(this, R.anim.slide_from_bottom)
        ivSplashBottomRightApptern.startAnimation(anim)
    }

    /** Go to next page*/
    private fun startActivity() {
        val intent = Intent(this@SplashActivity, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }
}
