package com.lastblade.ridemate.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewTreeObserver
import android.view.animation.AccelerateInterpolator
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.lastblade.ridemate.BuildConfig.APPLICATION_ID
import com.lastblade.ridemate.R
import com.lastblade.ridemate.interfaces.SimpleCallback
import com.lastblade.ridemate.util.D
import com.lastblade.ridemate.util.InitGeoLocationUpdate
import com.lastblade.ridemate.util.MapUtil
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.pick_up_destination_chooser.*
import kotlinx.android.synthetic.main.traveling_way_selection.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    private var googleMap: GoogleMap? = null
    private val TAG = MainActivity::class.java.simpleName
    /** Hold device's current location*/
    private var myCurrentLocation: LatLng? = null
    /** To get rid of from map nullable error
     * If map is not ready we wait or didn't allow the user for further work
     * */
    private var isMapReady = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        overridePendingTransition(R.anim.do_not_move, R.anim.do_not_move)

        initToolBar()
        initMap()
        initialActivityStartAnimation(savedInstanceState)

        ivUserCurrentLocation.setOnClickListener {
            if (isMapReady)
                myCurrentLocation?.let { loc ->
                    if (ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED
                            && ContextCompat.checkSelfPermission(this@MainActivity, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED)
                        googleMap?.isMyLocationEnabled = true
                    moveCameraToCurrentLocation(loc)
                }
        }

        ivArrowHide.setOnClickListener {
            if (wayContainer.visibility == View.VISIBLE && infoContainer.visibility == View.VISIBLE) {
                wayContainer.visibility = View.GONE
                infoContainer.visibility = View.GONE
                ivArrowHide.setImageResource(R.drawable.ic_keyboard_arrow_up)
            }else {
                wayContainer.visibility = View.VISIBLE
                infoContainer.visibility = View.VISIBLE
                ivArrowHide.setImageResource(R.drawable.ic_keyboard_arrow_down)
            }
        }
    }

    /** Move the camera position to device current location*/
    private fun moveCameraToCurrentLocation(loc: LatLng) {
        googleMap?.clear()
        val cuf = CameraUpdateFactory.newLatLngZoom(LatLng(loc.latitude, loc.longitude), 15f)
        googleMap?.animateCamera(cuf)
        addMarkerToMap(loc)

        addSomeStaticCarNearMyCurrentLoc(loc)
    }

    private fun addSomeStaticCarNearMyCurrentLoc(loc: LatLng) {
        var i = .003
        (0 until 2).forEach {
            googleMap?.addMarker(MarkerOptions().position(LatLng(loc.latitude + i,
                    loc.longitude + i))
                    .flat(true)
                    .icon(MapUtil.markerBitmap(
                            this@MainActivity, R.drawable.car))
                    .anchor(0.5f, 0.5f))

            i += .003
        }
    }

    /** Adding marker to the map*/
    private fun addMarkerToMap(loc: LatLng) {
        val latLng = LatLng(loc.latitude, loc.longitude)
        val marker = googleMap?.addMarker(MarkerOptions()
                .position(latLng))
        marker?.showInfoWindow()
    }

    /** Initialize map for the view*/
    private fun initMap() {
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.mapDelivery) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /** Override to handling google map or to get map instance*/
    override fun onMapReady(map: GoogleMap?) {
        map?.let {
            googleMap = map
            isMapReady = true
        }
    }

    /** Override to invoke location permission*/
    override fun onResume() {
        super.onResume()

        checkingLocationPermission()
    }

    /** Invoking to get run time location permission from user*/
    private fun checkingLocationPermission() {
        Dexter.withActivity(this).withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        initLocationCall()
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        handOnPermissionDenied(response)
                    }

                    @RequiresApi(api = Build.VERSION_CODES.M)
                    override fun onPermissionRationaleShouldBeShown(permission: PermissionRequest, token: PermissionToken) {
                        token.continuePermissionRequest()
                    }
                }).check()
    }

    /** Variable for permission setting request*/
    private val LOCATION_PERMISSION_SETTING_REQUEST = 99
    /** Variable to show denied dialog once*/
    private var permissionDeniedDialogShown: Boolean = false

    /** Handling permission denied*/
    private fun handOnPermissionDenied(response: PermissionDeniedResponse) {
        if (response.isPermanentlyDenied && !permissionDeniedDialogShown) {
            permissionDeniedDialogShown = true

            showingSnackBarToOpenLocationPermissionSetting()
        }
    }

    /** Snackbar for location permission denied*/
    private fun showingSnackBarToOpenLocationPermissionSetting() {
        D.showSnackbar(this, R.string.permission_location_manager_denied,
                R.string.ok, View.OnClickListener {
            val intent = Intent()

            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            intent.data = Uri.fromParts("package", APPLICATION_ID, null)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivityForResult(intent, LOCATION_PERMISSION_SETTING_REQUEST)
        })
    }

    /** Initializing location update*/
    private fun initLocationCall() {
        InitGeoLocationUpdate.locationInit(this, object : SimpleCallback<LatLng> {
            override fun callback(`object`: LatLng) {
                val latitude = `object`.latitude
                val longitude = `object`.longitude
                Log.d(TAG, "Current Location Latitude: " + latitude + " Longitude: " +
                        longitude)

                myCurrentLocation = `object`
            }
        })
    }

    /** Override to stop location update*/
    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    /** Stopping location update */
    private fun stopLocationUpdates() {
        InitGeoLocationUpdate.stopLocationUpdate(this)
    }

    /**
     * Checking device Os version
     * if os is greater than kitkat then we reveal circular for this activity
     * [savedInstanceState] savedInstanceState*/
    private fun initialActivityStartAnimation(savedInstanceState: Bundle?) {
        if (savedInstanceState == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            rootMainActivityLayout.visibility = View.INVISIBLE

            val viewTreeObserver = rootMainActivityLayout.viewTreeObserver
            if (viewTreeObserver.isAlive) {
                viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                    override fun onGlobalLayout() {
                        revealCircularAnim()
                        rootMainActivityLayout.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    }
                })
            }
        }
    }

    /** Implemented circular reveal
     * Circular reveal when activity first start*/
    private fun revealCircularAnim() {
        val cx = rootMainActivityLayout.width / 2
        val cy = rootMainActivityLayout.height / 2
        val finalRadius = Math.max(rootMainActivityLayout.width, rootMainActivityLayout.height)

        // create the animator for this view (the start radius is zero)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val circularReveal = ViewAnimationUtils.createCircularReveal(rootMainActivityLayout,
                    cx, cy, 0f, finalRadius.toFloat())
            circularReveal.duration = 400
            circularReveal.interpolator = AccelerateInterpolator()

            // make the view visible and start the animation
            rootMainActivityLayout.visibility = View.VISIBLE
            circularReveal.start()
        } else {
            return
        }
    }

    /** Initializing toolbar*/
    private fun initToolBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        actionBar?.setHomeButtonEnabled(true)
        actionBar?.setDisplayHomeAsUpEnabled(true)

        actionBar?.title = getString(R.string.confirmation)
        toolbar?.setTitleTextColor(android.graphics.Color.WHITE)
    }
}
