package com.lastblade.ridemate.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.support.v4.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

/**
 * Created by Shaon on 10/4/2018.
 * Bismillahir Rohmanir Rohim :)
 */
object MapUtil {

    fun markerBitmap(context: Context, drawable: Int): BitmapDescriptor? {
        val height = 80
        val width = 80
        val bitMapDraw = ContextCompat.getDrawable(context, drawable) as BitmapDrawable
        val b = bitMapDraw.bitmap
        val imageBitmap = Bitmap.createScaledBitmap(b, width, height, false)
        return BitmapDescriptorFactory
                .fromBitmap(imageBitmap)

    }
}