package com.lastblade.ridemate.util

import android.app.Activity
import android.support.design.widget.Snackbar
import android.view.View

/**
 * Created by Shaon on 10/3/2018.
 * Bismillahir Rohmanir Rohim :)
 */
object D {
    fun showSnackbar(activity: Activity, snackMsgId: Int,
                     actionStrId: Int, listener: View.OnClickListener?) {
        val snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity
                .getString(snackMsgId), Snackbar.LENGTH_INDEFINITE)
        if (actionStrId != 0 && listener != null) {
            snackbar.setAction(activity.getString(actionStrId), listener)
        }
        snackbar.show()
    }
}