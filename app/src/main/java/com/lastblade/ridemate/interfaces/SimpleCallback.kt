package com.lastblade.ridemate.interfaces

/**
 * Created by touhid on 7/8/16.
 *
 * @author touhid
 */
interface SimpleCallback<T> {
    fun callback(`object`: T)
}
